package ru.t1.sochilenkov.tm.command.data;

import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.dto.Domain;
import ru.t1.sochilenkov.tm.enumerated.Role;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Load data from backup file.";

    @NotNull
    public static final String NAME = "backup-load";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final byte[] base64Byte = Files.readAllBytes(Paths.get(FILE_BACKUP));
        @Nullable final String base64Date = new String(base64Byte);
        @Nullable final byte[] bytes = Base64.getDecoder().decode(base64Date);
        @Cleanup @NotNull final ByteArrayInputStream byteArrayInputStream =  new ByteArrayInputStream(bytes);
        @Cleanup @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
    }

}
